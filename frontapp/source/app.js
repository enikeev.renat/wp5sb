import './assets/scss/main.scss'


(() => {
	class Main {
		constructor(a, b) {
			this.a = a;
			this.b = b
		}

		getSum() {
			return this.a + this.b
		}
	}


	let main = new Main(1,5)

	console.info(main.getSum())
})()
