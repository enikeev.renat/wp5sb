const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const PATHS = {
	src: path.resolve(__dirname, '../source'),
	dist: path.resolve(__dirname, '../../dist'),
}
console.log('PATH')
console.log(__dirname)


module.exports = (env, options) => {
	console.log(`Mode: ${options.mode}`)

	const PROD = options.mode === 'production'
	const DEV = options.mode === 'development'

	return {
		mode: options.mode,

		entry: {
			app: [`${PATHS.src}/app.js`],
		},

		output: {
			path: PATHS.dist,
			filename: 'js/[name].js',
			clean: true,
			//publicPath: '/dist/',
		},

		devtool: 'eval-cheap-module-source-map',

		devServer: {
			static: path.join(PATHS.src, 'static')
		},

		plugins: [
			new CopyPlugin({
				patterns: [
					{ from: path.join(PATHS.src, 'static'), to: PATHS.dist },
				],
			}),
			new webpack.ProvidePlugin({
				$: "jquery",
				jQuery: "jquery",
				"window.jQuery": "jquery"
			}),
			new webpack.DefinePlugin({
				'PROD': JSON.stringify(PROD),
				'DEV': JSON.stringify(DEV),
			}),
			new MiniCssExtractPlugin({
				filename: `css/[name].css`
			}),
			...fs.readdirSync(`${PATHS.src}/view/pages/`).map(section => {
				return fs
					.readdirSync(`${PATHS.src}/view/pages/${section}/`)
					.filter(name => {
						return path.extname(name) === '.pug'
					})
					.map(name => {
						const filename = path.parse(name).name;
						return new HtmlWebpackPlugin({
							filename: DEV
								? `${section}${filename === 'index' ? '' : '-' + filename}.html`
								: `html/${section}${filename === 'index' ? '' : '-' + filename}.html`
							,
							template: `${PATHS.src}/view/pages/${section}/${filename}.pug`,
							inject: 'body',
							minify: false,
						});
					})
			}).flat(),
		],

		module: {
			rules: [
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						DEV ? 'style-loader' : MiniCssExtractPlugin.loader,
						'css-loader',
						{
							loader: 'postcss-loader',
							options: {
								postcssOptions: {
									plugins: [
										[
											'postcss-preset-env',
											{
												// options
											}
										]
									]
								}
							}
						},
						'resolve-url-loader',
						{
							loader: 'sass-loader',
							options: {
								sourceMap: true,
								additionalData: `
$PROD: ${PROD};
$DEV: ${DEV};
@import "~/assets/scss/_mixins.scss";
@import "~/assets/scss/_variables.scss";
								`
							}
						},
					]
				},
				{
					test: /\.pug$/i,
					use: [
						{
							loader: 'html-loader',
							options: {
								sources: {
									urlFilter: (attribute, value, resourcePath) => {
										return !path.isAbsolute(value)
									}
								}
							}
						},
						{
							loader: 'pug-html-loader',
							options: {
								pretty: true,
							}
						},
					],
					exclude: /node_modules/,
				},
				{
					test: /\.(gif|png|jpe?g|svg)$/i,
					type: 'asset/resource',
					generator: {
						filename: (file) => {
							return 'img/[name][ext]'
						},
					},
				},
				{
					test: /\.(woff(2)?|ttf|eot)$/i,
					type: 'asset/resource',
					generator: {
						filename: (file) => {
							return 'fonts/[name][ext]'
						},
					},
				},
				{
					test: /\.m?js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env']
						}
					},
				},
			]
		},

		resolve: {
			extensions: ['.tsx', '.ts', '.js', '.pug'],
			alias: {
				"~": PATHS.src,
			}
		},
	};
}
